<?php

namespace App\Tests;

use App\Dto\DiscountCreateInput;
use App\Dto\ProductCreateInput;
use App\Dto\UserCreateInput;
use App\Entity\Country;
use App\Entity\DeliveryInfo;
use App\Entity\Discount;
use App\Entity\Product;
use App\Entity\User;
use App\Service\DiscountManager;
use App\Service\OrderManager;
use App\Service\ProductManager;
use App\Service\UserManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class OrderItemCalculationTest extends KernelTestCase
{
    /** @var OrderManager  */
    private $orderManager;

    /** @var UserManager  */
    private $userManager;

    /** @var ProductManager  */
    private $productManager;

    /** @var DiscountManager  */
    private $discountManager;

    private $user;
    private $product;

    protected function setUp(): void
    {
        $this->bootKernel();
        $this->orderManager = self::$container->get(OrderManager::class);
        $this->userManager = self::$container->get(UserManager::class);
        $this->productManager = self::$container->get(ProductManager::class);
        $this->discountManager = self::$container->get(DiscountManager::class);
    }

    private function createUser()
    {
        $userInput = new UserCreateInput();
        $userInput->setPhone('09352352')
            ->setEmail('unittestemail@gmail.com')
            ->setFirstName('unit userFirstname')
            ->setLastName('unit userLastname');
        $this->user = $this->userManager->createUser($userInput);
        $this->assertTrue($this->user instanceof User);
    }

    private function createProduct()
    {
        $productInput = new ProductCreateInput();
        $productInput->setType(Product::TYPE_MUG)
            ->setInternationalCost(5)
            ->setCost(2)
            ->setUser($this->user->getId())
            ->setTitle('Title')
            ->setSku('123-412-sku');

        $this->product = $this->productManager->createProduct($productInput);

        $this->assertTrue($this->product instanceof Product);
    }

    private function createDiscount()
    {
        $discountInput = new DiscountCreateInput();
        $discountInput->setMinQuantity(1)
            ->setFactor(0.5)
            ->setProduct($this->product->getId());

        $discount = $this->discountManager->createDiscount($discountInput);

        $this->assertTrue($discount instanceof Discount);
    }

    public function testOrderMugCost()
    {
        $this->createUser();
        $this->createProduct();
        $this->createDiscount();

        $discount = new Discount();
        $discount->setMinQuantity(1)
            ->setFactor(0.5)
            ->setProduct($this->product);

        $deliveryInfoMock = $this->createMock(DeliveryInfo::class);
        $deliveryInfoMock->expects($this->once())
            ->method('getType')
            ->willReturn(DeliveryInfo::TYPE_STANDARD);
        $countryMock = $this->createMock(Country::class);
        $countryMock->expects($this->once())
            ->method('getCode')
            ->willReturn(Country::LOCAL_COUNTRY_CODE);
        $deliveryInfoMock->expects($this->once())
            ->method('getCountry')
            ->willReturn($countryMock);

        $orderItem = $this->orderManager->createOrderItem($deliveryInfoMock, [
            'product' => $this->product->getId(),
            'quantity' => 10
        ]);

        $this->assertTrue($orderItem->getTotal() == 11);
    }
}