<?php

namespace App\Service;

use App\Dto\DeliveryInfoInput;
use App\Dto\OrderCreateInput;
use App\Entity\Country;
use App\Entity\DeliveryInfo;
use App\Entity\Discount;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Product;
use App\Entity\User;
use App\Exception\ValidationException;
use App\Exception\ValidationViolationsException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OrderManager
{

    private EntityManagerInterface $em;
    private ValidatorInterface $validator;
    private DiscountManager $discountManager;

    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator, DiscountManager $discountManager)
    {
        $this->em = $em;
        $this->validator = $validator;
        $this->discountManager = $discountManager;
    }

    public function createOrder(OrderCreateInput $createInput): Order
    {
        $order = new Order();
        $user = null;
        if ($createInput->getUser()) {
            /** @var User $user */
            $user = $this->em->getRepository(User::class)->find($createInput->getUser());
        }

        if (!$user) {
            throw new ValidationException('user');
        }

        $order->setUser($user);
        $this->addOrderDeliveryInfo($order, $createInput->getDeliveryInfo());
        $this->addOrderItemsData($order, $createInput->getOrderItems());
        $this->calculateAndChargeOrderCost($order);

        $this->em->persist($order);
        $this->em->flush();

        return $order;
    }

    public function calculateAndChargeOrderCost(Order $order): void
    {
        $orderItems = $order->getOrderItems();
        $total = 0;
        foreach ($orderItems as $orderItem) {
            $total += $orderItem->getTotal();
        }
        $order->setTotal($total);
        $user = $order->getUser();
        if ($user->getBalance() < $total) {
            throw new ValidationException('', 'Not enough costs in balance');
        }
        $newUserBalance = round($user->getBalance() - $total, 2);
        $user->setBalance($newUserBalance);
    }

    public function addOrderDeliveryInfo(Order $order, DeliveryInfoInput $deliveryInfoInput): void
    {
        $country = null;
        if ($deliveryInfoInput->getCountry()) {
            /** @var Country $country */
            $country = $this->em->getRepository(Country::class)->find($deliveryInfoInput->getCountry());
        }

        if (!$country) {
            throw new ValidationException('deliveryInfo.country');
        }

        $deliveryInfo = new DeliveryInfo();
        $deliveryInfo->setType($deliveryInfoInput->getType())
            ->setFullName($deliveryInfoInput->getFullName())
            ->setPhone($deliveryInfoInput->getPhone())
            ->setStreet($deliveryInfoInput->getStreet())
            ->setCountry($country)
            ->setZip($deliveryInfo->getZip())
            ->setState($deliveryInfo->getState());

        $violations = $this->validator->validate($deliveryInfo);
        if ($violations->count() > 0) {
            throw new ValidationViolationsException($violations);
        }
        $this->em->persist($deliveryInfo);
        $order->setDeliveryInfo($deliveryInfo);
    }

    public function addOrderItemsData(Order $order, array $orderItems): void
    {
        if (!$orderItems) {
            throw new ValidationException('orderItems');
        }

        foreach ($orderItems as $orderItemData) {
            $orderItem = $this->createOrderItem($order->getDeliveryInfo(), $orderItemData);
            $order->addOrderItem($orderItem);
        }
    }

    public function createOrderItem(DeliveryInfo $deliveryInfo, array $orderItemData): OrderItem
    {
        if ((!isset($orderItemData['product']) || !$orderItemData['product']) ||
            (!isset($orderItemData['quantity']) || !$orderItemData['quantity'])) {
            throw new ValidationException('orderItems');
        }
        /** @var Product $product */
        $product = $this->em->getRepository(Product::class)->find($orderItemData['product']);
        if (!$product) {
            throw new ValidationException('orderItems.product');
        }
        $orderItem = new OrderItem();
        $orderItem->setProduct($product)
            ->setQuantity($orderItemData['quantity']);

        if ($deliveryInfo->getCountry()->getCode() === Country::LOCAL_COUNTRY_CODE) {
            $orderItem->setCost($product->getCost());
        } else {
            $orderItem->setCost($product->getInternationalCost());
        }

        $this->calculateOrderItemCost($orderItem, $deliveryInfo->getType());

        return $orderItem;
    }

    public function calculateOrderItemCost(OrderItem $orderItem, string $deliveryType): void
    {
        $discount = $this->em->getRepository(Discount::class)->findOneBy(['product' => $orderItem->getProduct()]);
        $total = $orderItem->getCost() * $orderItem->getQuantity();
        /** @var Discount $discount */
        if ($discount && $orderItem->getQuantity() > $discount->getMinQuantity()) {
            $orderItem->setDiscountFactor($discount->getFactor())
                ->setDiscountMinQuantity($discount->getMinQuantity());
            $discountAmount = $this->discountManager->calculateDiscountAmount($discount, $orderItem->getQuantity(), $orderItem->getCost());
            $total -= $discountAmount;
        }

        $deliveryCost = 0;
        if ($deliveryType == DeliveryInfo::TYPE_EXPRESS) {
            $deliveryCost = $orderItem->getQuantity() * DeliveryInfo::DELIVERY_EXPRESS_COST;
        }
        $orderItem->setDeliveryCost($deliveryCost);
        $total += $deliveryCost;

        $orderItem->setTotal(round($total, 2));
    }
}