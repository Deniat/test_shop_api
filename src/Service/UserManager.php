<?php

namespace App\Service;

use App\Dto\UserCreateInput;
use App\Entity\User;
use App\Exception\ValidationViolationsException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserManager
{

    private EntityManagerInterface $em;
    private ValidatorInterface $validator;

    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator)
    {

        $this->em = $em;
        $this->validator = $validator;
    }

    public function createUser(UserCreateInput $createInput): User
    {
        $user = new User();
        $user->setFirstName($createInput->getFirstName())
            ->setLastName($createInput->getLastName())
            ->setEmail($createInput->getEmail())
            ->setPhone($createInput->getPhone());

        $violations = $this->validator->validate($user);
        if ($violations->count() > 0) {
            throw new ValidationViolationsException($violations);
        }

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }
}