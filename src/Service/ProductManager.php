<?php

namespace App\Service;

use App\Dto\ProductCreateInput;
use App\Entity\Product;
use App\Entity\User;
use App\Exception\ValidationException;
use App\Exception\ValidationViolationsException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductManager
{

    private EntityManagerInterface $em;
    private ValidatorInterface $validator;

    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    public function createProduct(ProductCreateInput $createInput): Product
    {
        $product = new Product();
        $user = null;
        if ($createInput->getUser()) {
            /** @var User $user */
            $user = $this->em->getRepository(User::class)->find($createInput->getUser());
        }

        if (!$user) {
            throw new ValidationException('user');
        }
        $product->setUser($user)
            ->setCost($createInput->getCost())
            ->setInternationalCost($createInput->getInternationalCost())
            ->setType($createInput->getType())
            ->setSku($createInput->getSku())
            ->setTitle($createInput->getTitle());

        $violations = $this->validator->validate($product);
        if ($violations->count() > 0) {
            throw new ValidationViolationsException($violations);
        }

        $this->em->persist($product);
        $this->em->flush();

        return $product;
    }
}