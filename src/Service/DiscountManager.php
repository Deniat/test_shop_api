<?php

namespace App\Service;

use App\Dto\DiscountCreateInput;
use App\Entity\Discount;
use App\Entity\Product;
use App\Exception\ValidationException;
use App\Exception\ValidationViolationsException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DiscountManager
{

    private EntityManagerInterface $em;
    private ValidatorInterface $validator;

    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    public function createDiscount(DiscountCreateInput $createInput): Discount
    {
        $discount = new Discount();
        $product = null;
        if ($createInput->getProduct()) {
            /** @var Product $product */
            $product = $this->em->getRepository(Product::class)->find($createInput->getProduct());
        }

        if (!$product) {
            throw new ValidationException('product');
        }
        $discount->setProduct($product)
            ->setFactor($createInput->getFactor())
            ->setMinQuantity($createInput->getMinQuantity());

        $violations = $this->validator->validate($discount);
        if ($violations->count() > 0) {
            throw new ValidationViolationsException($violations);
        }

        $this->em->persist($discount);
        $this->em->flush();

        return $discount;
    }

    public function calculateDiscountAmount(Discount $discount, int $quantity, string $cost)
    {
        return (($quantity - $discount->getMinQuantity()) * $cost) * $discount->getFactor();
    }
}