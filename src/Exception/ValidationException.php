<?php

namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ValidationException extends BadRequestHttpException
{
    private ?string $property;

    public function __construct(?string $property = null, $message = "Bad Request", $code = 400, \Throwable $previous = null)
    {
        $this->property = $property;
        parent::__construct($message, $previous, $code);
    }

    public function getProperty(): ?string
    {
        return $this->property;
    }
}