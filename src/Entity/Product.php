<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class Product implements TimestampableInterface
{
    use TimestampableFieldsTrait;
    use TimestampableMethodsTrait;

    const TYPE_T_SHIRT = 't_shirt';
    const TYPE_MUG = 'mug';
    const AVAILABLE_TYPES = [
        self::TYPE_T_SHIRT,
        self::TYPE_MUG
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"View"})
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotNull()
     * @Groups({"View"})
     */
    private ?string $title;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotNull()
     * @Groups({"View"})
     */
    private ?string $sku;

    /**
     * @ORM\Column(type="string")
     * @Assert\Choice(choices=Product::AVAILABLE_TYPES)
     * @Assert\NotNull()
     */
    private ?string $type;

    /**
     * @ORM\Column(type="decimal", precision=22, scale=2)
     * @Assert\GreaterThan(0)
     * @Groups({"View"})
     */
    private ?string $cost;

    /**
     * @ORM\Column(type="decimal", precision=22, scale=2)
     * @Assert\GreaterThan(0)
     * @Groups({"View"})
     */
    private ?string $internationalCost;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private User $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;
        return $this;
    }

    public function getSku(): string
    {
        return $this->sku;
    }

    public function setSku(?string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getCost(): string
    {
        return $this->cost;
    }

    public function setCost(?string $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getInternationalCost(): string
    {
        return $this->internationalCost;
    }

    public function setInternationalCost(?string $internationalCost): self
    {
        $this->internationalCost = $internationalCost;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
