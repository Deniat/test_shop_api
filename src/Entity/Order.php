<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class Order implements TimestampableInterface
{
    use TimestampableFieldsTrait;
    use TimestampableMethodsTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"View"})
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     *  @Groups({"View"})
     *
     * @Assert\NotNull()
     */
    private User $user;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\DeliveryInfo")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Assert\NotNull()
     */
    private DeliveryInfo $deliveryInfo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderItem", mappedBy="order", orphanRemoval=true, cascade={"persist"})
     * @Groups({"View"})
     */
    private Collection $orderItems;

    /**
     * @ORM\Column(type="decimal", precision=22, scale=2)
     * @Groups({"View"})
     */
    private string $total;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __construct()
    {
        $this->orderItems = new ArrayCollection();
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDeliveryInfo(): DeliveryInfo
    {
        return $this->deliveryInfo;
    }

    public function setDeliveryInfo(DeliveryInfo $deliveryInfo): self
    {
        $this->deliveryInfo = $deliveryInfo;

        return $this;
    }

    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItem $orderItem): self
    {
        $this->orderItems->add($orderItem);
        $orderItem->setOrder($this);

        return $this;
    }

    public function removeOrderItem(OrderItem $orderItem): self
    {
        $this->orderItems->removeElement($orderItem);
        //todo recalculate total

        return $this;
    }

    public function getTotal(): string
    {
        return $this->total;
    }

    public function setTotal(string $total): self
    {
        $this->total = $total;

        return $this;
    }
}
