<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class User implements TimestampableInterface
{
    use TimestampableFieldsTrait;
    use TimestampableMethodsTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"View"})
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull()
     * @Groups({"View"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull()
     * @Groups({"View"})
     */
    private $lastName;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull()
     * @Assert\Email()
     * @Groups({"View"})
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull()
     * @Groups({"View"})
     */
    private $phone;

    /**
     * @ORM\Column(type="decimal", precision=22, scale=2, options={"default":100.00})
     * @Groups({"View"})
     */
    private $balance = 100.00;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getBalance(): ?string
    {
        return $this->balance;
    }

    public function setBalance(string $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }
}
