<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class Discount implements TimestampableInterface
{
    use TimestampableFieldsTrait;
    use TimestampableMethodsTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"View"})
     */
    private ?int $id;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=4)
     * @Groups({"View"})
     * @Assert\LessThan(1)
     */
    private string $factor;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"View"})
     * @Assert\NotNull()
     */
    private int $minQuantity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"View"})
     */
    private Product $product;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFactor(): string
    {
        return $this->factor;
    }

    public function setFactor(string $factor): self
    {
        $this->factor = $factor;

        return $this;
    }

    public function getMinQuantity(): int
    {
        return $this->minQuantity;
    }

    public function setMinQuantity(int $minQuantity): self
    {
        $this->minQuantity = $minQuantity;

        return $this;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }
}
