<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableMethodsTrait;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 */
class OrderItem implements TimestampableInterface
{
    use TimestampableFieldsTrait;
    use TimestampableMethodsTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Order")
     * @ORM\JoinColumn(nullable=false)
     */
    private Order $order;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"View"})
     */
    private Product $product;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"View"})
     */
    private int $quantity = 1;

    /**
     * if product price updated, we need to keep price on the moment of order creation
     * @ORM\Column(type="decimal", precision=22, scale=2)
     * @Groups({"View"})
     * @Groups({"View"})
     */
    private string $cost;

    /**
     * @ORM\Column(type="decimal", precision=22, scale=2)
     */
    private string $deliveryCost;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $discountMinQuantity = 0;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=4, nullable=true)
     */
    private ?string $discountFactor;

    /**
     * @ORM\Column(type="decimal", precision=22, scale=2)
     * @Groups({"View"})
     */
    private string $total;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function setOrder(Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getCost(): string
    {
        return $this->cost;
    }

    public function setCost(string $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getDeliveryCost(): string
    {
        return $this->deliveryCost;
    }

    public function setDeliveryCost(string $deliveryCost): self
    {
        $this->deliveryCost = $deliveryCost;

        return $this;
    }

    public function getDiscountMinQuantity(): int
    {
        return $this->discountMinQuantity;
    }

    public function setDiscountMinQuantity(int $discountMinQuantity): self
    {
        $this->discountMinQuantity = $discountMinQuantity;

        return $this;
    }

    public function getDiscountFactor(): string
    {
        return $this->discountFactor;
    }

    public function setDiscountFactor(string $discountFactor): self
    {
        $this->discountFactor = $discountFactor;

        return $this;
    }

    public function getTotal(): string
    {
        return $this->total;
    }

    public function setTotal(string $total): self
    {
        $this->total = $total;

        return $this;
    }
}
