<?php

namespace App\Dto;

class DiscountCreateInput
{
    private $product;
    private $minQuantity;
    private $factor;

    public function getProduct(): ?string
    {
        return $this->product;
    }

    public function setProduct(?string $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getMinQuantity(): ?string
    {
        return $this->minQuantity;
    }

    public function setMinQuantity(?string $minQuantity): self
    {
        $this->minQuantity = $minQuantity;

        return $this;
    }

    public function getFactor(): ?string
    {
        return $this->factor;
    }

    public function setFactor(?string $factor): self
    {
        $this->factor = $factor;

        return $this;
    }
}