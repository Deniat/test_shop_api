<?php

namespace App\Dto;

class OrderCreateInput
{
    private $user;
    private $deliveryInfo;
    private $orderItems = [];

    public function getUser(): ?string
    {
        return $this->user;
    }

    public function setUser(?string $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDeliveryInfo(): ?DeliveryInfoInput
    {
        return $this->deliveryInfo;
    }

    public function setDeliveryInfo(?DeliveryInfoInput $deliveryInfo): self
    {
        $this->deliveryInfo = $deliveryInfo;

        return $this;
    }

    public function getOrderItems(): array
    {
        return $this->orderItems;
    }

    public function setOrderItems(array $orderItems): self
    {
        $this->orderItems = $orderItems;

        return $this;
    }
}