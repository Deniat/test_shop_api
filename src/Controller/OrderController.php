<?php

namespace App\Controller;

use App\Dto\OrderCreateInput;
use App\Service\OrderManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class OrderController extends AbstractController
{
    use ApiTrait;

    private SerializerInterface $serializer;
    private OrderManager $orderManager;

    public function __construct(OrderManager $orderManager, SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
        $this->orderManager = $orderManager;
    }

    /**
     * @Route("/orders", name="api_order_create", methods={"POST"})
     */
    public function createAction(Request $request)
    {
        /** @var OrderCreateInput $orderCreateInput */
        $orderCreateInput = $this->createDtoObject($request->getContent(), OrderCreateInput::class);
        $order = $this->orderManager->createOrder($orderCreateInput);
        return new JsonResponse($this->createResponseObject($order), Response::HTTP_CREATED, [], true);
    }
}