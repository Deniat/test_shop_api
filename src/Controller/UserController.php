<?php

namespace App\Controller;

use App\Dto\UserCreateInput;
use App\Entity\Product;
use App\Entity\User;
use App\Service\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class UserController extends AbstractController
{
    use ApiTrait;

    private SerializerInterface $serializer;
    private UserManager $userManager;
    private EntityManagerInterface $em;

    public function __construct(UserManager $userManager, SerializerInterface $serializer, EntityManagerInterface $em)
    {
        $this->serializer = $serializer;
        $this->userManager = $userManager;
        $this->em = $em;
    }

    /**
     * @Route("/users", name="api_user_create", methods={"POST"})
     */
    public function createAction(Request $request)
    {
        /** @var UserCreateInput $userCreateInput */
        $userCreateInput = $this->createDtoObject($request->getContent(), UserCreateInput::class);
        $user = $this->userManager->createUser($userCreateInput);
        return new JsonResponse($this->createResponseObject($user), Response::HTTP_CREATED, [], true);
    }

    /**
     * @Route("/users/{user}/products", name="api_user_get_products", methods={"GET"}, requirements={"user":"\d+"})
     */
    public function getUserProductsAction(Request $request, User $user)
    {
        $products = $this->em->getRepository(Product::class)->findBy(['user' => $user]);
        return new JsonResponse($this->createResponseObject($products), Response::HTTP_OK, [], true);
    }
}