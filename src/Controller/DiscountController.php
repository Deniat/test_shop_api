<?php

namespace App\Controller;

use App\Dto\DiscountCreateInput;
use App\Service\DiscountManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class DiscountController extends AbstractController
{
    use ApiTrait;

    private SerializerInterface $serializer;
    private DiscountManager $discountManager;

    public function __construct(DiscountManager $discountManager, SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
        $this->discountManager = $discountManager;
    }

    /**
     * @Route("/discounts", name="api_discount_create", methods={"POST"})
     */
    public function createAction(Request $request)
    {
        /** @var DiscountCreateInput $discountCreateInput */
        $discountCreateInput = $this->createDtoObject($request->getContent(), DiscountCreateInput::class);
        $discount = $this->discountManager->createDiscount($discountCreateInput);
        return new JsonResponse($this->createResponseObject($discount), Response::HTTP_CREATED, [], true);
    }
}