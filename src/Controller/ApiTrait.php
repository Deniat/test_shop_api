<?php

namespace App\Controller;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

trait ApiTrait
{
    private function createDtoObject(?string $data, string $dtoClass): object
    {
        try {
            return $this->serializer->deserialize($data, $dtoClass, 'json');
        } catch (\Throwable $e) {
            throw new BadRequestHttpException();
        }
    }

    private function createResponseObject($data, string $group = 'View'): string
    {
        return $this->serializer->serialize($data, 'json', ['groups' => $group]);
    }
}