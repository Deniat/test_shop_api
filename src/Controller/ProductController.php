<?php

namespace App\Controller;

use App\Dto\ProductCreateInput;
use App\Service\ProductManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ProductController extends AbstractController
{
    use ApiTrait;

    private SerializerInterface $serializer;
    private ProductManager $productManager;

    public function __construct(ProductManager $productManager, SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
        $this->productManager = $productManager;
    }

    /**
     * @Route("/products", name="api_product_create", methods={"POST"})
     */
    public function createAction(Request $request)
    {
        /** @var ProductCreateInput $productCreateInput */
        $productCreateInput = $this->createDtoObject($request->getContent(), ProductCreateInput::class);
        $product = $this->productManager->createProduct($productCreateInput);
        return new JsonResponse($this->createResponseObject($product), Response::HTTP_CREATED, [], true);
    }
}