<?php

namespace App\Validator;

use App\Entity\Country;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class DeliveryInfoValidator extends ConstraintValidator
{

    public function validate($deliveryInfo, Constraint $constraint)
    {
        if (!$constraint instanceof DeliveryInfo) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\DeliveryInfo');
        }

        if (null === $deliveryInfo) {
            return;
        }

        /** @var \App\Entity\DeliveryInfo $deliveryInfo */
        if ($deliveryInfo->getCountry()->getCode() === Country::LOCAL_COUNTRY_CODE) {
            if (!$deliveryInfo->getState()) {
                $this->addError('state', "Not valid");
            }

            if (!$deliveryInfo->getZip()) {
                $this->addError('zip', "Not valid");
            }
        } else {
            if ($deliveryInfo->getType() == \App\Entity\DeliveryInfo::TYPE_EXPRESS) {
                $this->addError('type', sprintf('Express delivery is only possible for %s', Country::LOCAL_COUNTRY_CODE));
            }
        }
    }

    private function generatePath(string $property)
    {
        return 'deliveryInfo.' . $property;
    }

    private function addError(string $property, string $message)
    {
        $this->context
            ->buildViolation($message)
            ->atPath($this->generatePath($property))
            ->addViolation();
    }
}
