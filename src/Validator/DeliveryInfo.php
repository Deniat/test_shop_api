<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DeliveryInfo extends Constraint
{
    public string $message = 'DeliveryInfo is not valid';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
