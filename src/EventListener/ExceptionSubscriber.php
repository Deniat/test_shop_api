<?php

namespace App\EventListener;

use App\Exception\ValidationException;
use App\Exception\ValidationViolationsException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Validator\ConstraintViolationInterface;

class ExceptionSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::EXCEPTION => 'onKernelException'
        );
    }

    public function onKernelException(ExceptionEvent $event)
    {
        // You get the exception object from the received event
        $e = $event->getThrowable();

        if ($e instanceof ValidationViolationsException) {
            $response = new JsonResponse($this->createBadRequestResponseFromViolations($e));
            $event->setResponse($response);
        } elseif ($e instanceof ValidationException) {
            $response = new JsonResponse($this->createBadRequestResponseFromException($e));
            $event->setResponse($response);
        }
    }

    private function createBadRequestResponseFromViolations(ValidationViolationsException $e): array
    {
        $response = $this->getResponseArray($e);
        /** @var ConstraintViolationInterface $violation */
        foreach ($e->getViolations() as $violation) {
            $error = [
                'property' => $violation->getPropertyPath(),
                'message' => $violation->getMessage()
            ];

            $response['errors'][] = $error;
        }

        return $response;
    }

    private function createBadRequestResponseFromException(ValidationException $e): array
    {
        $response = $this->getResponseArray($e);
        if ($property = $e->getProperty()) {
            $response['errors'][] = [
                'property' => $e->getProperty(),
                'message' => 'Not valid'
            ];
        }

        return $response;
    }

    private function getResponseArray(\Throwable $e): array
    {
        $response['message'] = $e->getMessage();
        $response['errors'] = [];

        return $response;
    }
}