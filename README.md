##This is a test project. 
Requirement was not to use ApiPlatform or FOSRestBundle.

Please see /tests for postman collection.

## Project initialization locally

##### Docker up
1. `docker-compose build` 
2. `docker-compose up -d`
3. `docker-compose exec php bash`

##### Project up
1. `composer install`
2. `bin/console doctrine:migration:migrate`
3. Check `.env` to create `.env.local` that fit to you

##### Testing
1. Run `bin/console doctrine:schema:update --force --env=test`
2. Run `bin/phpunit`