<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200820120050 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tbl_order_item CHANGE discount_min_quantity discount_min_quantity INT DEFAULT NULL, CHANGE discount_factor discount_factor NUMERIC(5, 4) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tbl_order_item CHANGE discount_min_quantity discount_min_quantity INT NOT NULL, CHANGE discount_factor discount_factor NUMERIC(5, 4) NOT NULL');
    }
}
