<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\Product;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\Intl\Countries;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200819102618 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('INSERT into tbl_user (`first_name`, `last_name`, `email`, `phone`) VALUES ("Andrew", "Skalkovych", "deniatnievolin@gmail.com", "+380939374374")');
        $this->addSql("SELECT id FROM tbl_user ORDER BY id DESC  LIMIT 1 INTO @lastInsertId;
                INSERT INTO tbl_product (`user_id`, `title`, `sku`, `cost`, `type`, `international_cost`)  VALUES (@lastInsertId, 'Some Mug', '123-mid-mug', 2.00, :mug, 5.00);
                INSERT INTO tbl_product (`user_id`, `title`, `sku`, `cost`, `type`, `international_cost`)  VALUES (@lastInsertId, 'Some T-shirt', '123-mid-tshirt', 1.00, :shirt, 3.00);",
            [
                'mug' => Product::TYPE_MUG,
                'shirt' => Product::TYPE_T_SHIRT,
            ]);
        $productIds = $this->connection->fetchAll('SELECT id FROM `tbl_product`');
        foreach ($productIds as $idData) {
            $id = $idData['id'];
            //insert discount
            $this->addSql('INSERT into tbl_discount (`product_id`, `min_quantity`, `factor`) VALUES (:id, 1, 0.5)', [
                'id' => $id
            ]);
        }

        $countriesEn = Countries::getNames("en");

        foreach ($countriesEn as $code => $title) {
            $this->addSql("INSERT INTO tbl_country (code, name, created_at, updated_at)  VALUES (:code, :name, :date, :date)", [
                'code' => $code,
                'name' => $title,
                'date' => (new \DateTime())->format('Y-m-d H:i:s')
            ]);

        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DELETE FROM tbl_discount');
        $this->addSql('DELETE FROM tbl_country');
    }
}
